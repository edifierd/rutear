import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { UsuarioRegistroComponent } from './usuario-registro/usuario-registro.component';

import { UsuarioService } from './service/usuario.service';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { UsuarioDetailComponent } from './usuario-detail/usuario-detail.component';
import { UsuarioIndexComponent } from './usuario-index/usuario-index.component';


const routes: Route[] = [
  {path: '', component: IndexComponent},
  {path: 'usuarios', component: UsuarioListComponent},
  {path: 'iniciousuario', component: UsuarioIndexComponent},
  {path: 'usuario/:id', component: UsuarioDetailComponent},
  {path: 'registro', component: UsuarioRegistroComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsuarioRegistroComponent,
    IndexComponent,
    UsuarioListComponent,
    UsuarioDetailComponent,
    UsuarioIndexComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [
    UsuarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

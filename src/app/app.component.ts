import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from './entity/usuario';
import { UsuarioService } from './service/usuario.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Rute.Ar';
  currentUser: Usuario;

  constructor(private usuarioService: UsuarioService, private router: Router){
    
  }

  ngOnInit() {
    console.log(this.router);
    if(localStorage.getItem('currentUser') != null){
      this.currentUser  = new Usuario();  
      this.currentUser.setDataFrom(localStorage.getItem('currentUser'));
      if(!this.currentUser.getAccess(this.router.url)){
        this.router.navigateByUrl('registro');
      }
    }
  }

  cerrar(){
    this.usuarioService.logout();
    this.currentUser = null;
    this.router.navigateByUrl('');
    console.log("cerrado");
  }
  
}

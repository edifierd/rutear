import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../entity/usuario';

import { HttpHeaders } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private httpClient: HttpClient) { 

  }

  addUsuario(usuario: Usuario){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return this.httpClient.post<Usuario>("http://localhost:8080/RuteAr/usuarios", usuario, httpOptions);

  }

  editUsuario(usuario: Usuario){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return this.httpClient.put("http://localhost:8080/RuteAr/usuarios", usuario, httpOptions);

  }

  listUsuarios(){
    return this.httpClient.get<Usuario[]>("http://localhost:8080/RuteAr/usuarios");
  }

  getUsuario(id: number){
    return this.httpClient.get<Usuario>("http://localhost:8080/RuteAr/usuarios/"+id);
  }

  tryLogin(username: string, password: string){
    return this.httpClient.post<Usuario>('http://localhost:8080/RuteAr/usuarios/login', {
      username: username,
      password: password
    });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

}

export class Rol {
     id: number;
     descripcion: string;
     urisPermitidas: Array<String>;

    constructor(rol){
        this.id = rol.id;
        this.descripcion = rol.descripcion;
        this.urisPermitidas = rol.urisPermitidas;
    }

    getUrisPermitidas(){
        return this.urisPermitidas;
    }
}

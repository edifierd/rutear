import { Rol } from '../entity/rol'; 

export class Usuario {
     id: number;
     username: string;
     password: string;
     dni: string;
     apellido: string;
     nombre: string;
     domicilio:string;
     sexo: string;
     email:string;
     fechaNacimiento: string;
     rol: Rol;
     habilitado: boolean;

    public setDataFrom(userData: string){
        var data = JSON.parse(userData);
        this.id = data.id;
        this.username = data.username;
        this.dni = data.dni;
        this.apellido = data.apellido;
        this.nombre = data.nombre;
        this.domicilio = data.domicilio;
        this.sexo = data.sexo;
        this.email = data.email;
        this.fechaNacimiento = data.fechaNacimiento;
        this.rol = new Rol(data.rol);
        this.habilitado = data.habilitado;
    }

    public getAccess(ruta: String){
        if(this.rol.getUrisPermitidas().indexOf(ruta) !== -1) {
           return true;
        }
        return false;
    }
}

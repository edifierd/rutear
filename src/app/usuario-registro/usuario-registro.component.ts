import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


import { Usuario } from '../entity/usuario';
import { UsuarioService } from '../service/usuario.service';
import { Rol } from '../entity/rol';

@Component({
  selector: 'app-usuario-registro',
  templateUrl: './usuario-registro.component.html',
  styleUrls: ['./usuario-registro.component.css']
})
export class UsuarioRegistroComponent implements OnInit {
  msj: string;
  usuario = new Usuario();

  constructor(private usuarioService: UsuarioService) {

  }

  ngOnInit() {
  }

  onSubmit(f: NgForm){
    this.msj = "";
    let aux = new Usuario();
    aux.username = f.value.username;
    aux.apellido = f.value.apellido;
    aux.nombre = f.value.nombre;
    aux.dni = f.value.dni;
    aux.domicilio = f.value.domicilio;
    aux.sexo = f.value.genero;
    aux.rol= new Rol({id: 1});
    aux.email = f.value.email;
    aux.password = f.value.password;
    this.usuarioService.addUsuario(aux).subscribe();
    f.resetForm();
    this.msj = "El usuario se ha creado correctamente";
  }

}

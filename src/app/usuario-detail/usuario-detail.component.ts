import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Usuario } from '../entity/usuario';
import { UsuarioService } from '../service/usuario.service';


@Component({
  selector: 'app-usuario-detail',
  templateUrl: './usuario-detail.component.html',
  styleUrls: ['./usuario-detail.component.css']
})
export class UsuarioDetailComponent implements OnInit {

  usuario= new Usuario;

  constructor(private route: ActivatedRoute,private usuarioService: UsuarioService) { 

  }

  ngOnInit() {
    let id: number;
     this.route.params.subscribe(params => {
      id = +params['id']; // (+) converts string 'id' to a number
    });
    this.usuarioService.getUsuario(id).subscribe(data => {
      this.usuario = data;
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import {UsuarioService} from '../service/usuario.service';
import {Router} from '@angular/router';
import { AppComponent } from '../app.component';
import { Usuario } from '../entity/usuario';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  msj: string;
  username: string;
  password: string;

  constructor(private usuarioService: UsuarioService, private router: Router, private parent: AppComponent) { }

  ngOnInit() {
    console.log("index.component");
    console.log(this.parent.currentUser);
    if(this.parent.currentUser){
      this.router.navigateByUrl('iniciousuario');
    }
  }

  onSubmit(f: NgForm){
    this.msj = "";
    this.usuarioService.tryLogin(this.username, this.password).subscribe(
      user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.parent.currentUser = new Usuario();  
        this.parent.currentUser.setDataFrom(localStorage.getItem('currentUser'));
        this.router.navigateByUrl('iniciousuario');
      },
      r => {
        this.msj = r.error.mensaje;
      }
    );
    f.resetForm();
  }

}

import { Component, OnInit } from '@angular/core';

import { Usuario } from '../entity/usuario';
import { UsuarioService } from '../service/usuario.service';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.css']
})
export class UsuarioListComponent implements OnInit {

  usuarios=[];

  constructor(private usuarioService: UsuarioService) {
    this.usuarioService.listUsuarios().subscribe(data => {
      this.usuarios = data;
    });
  }

  cambiarEstado(user: Usuario){
    user.habilitado = !user.habilitado;
    this.usuarioService.editUsuario(user).subscribe();
  }

  ngOnInit() {
  }

}
